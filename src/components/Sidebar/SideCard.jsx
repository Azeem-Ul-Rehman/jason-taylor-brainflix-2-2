import { Link } from 'react-router-dom';

function SideCard({ title, channel, poster, id }) {
    return (

        <section className="sidebar-content">
            <Link className ="sidebar-link"to={"/video/"+id}>
            <div style = {{display: 'flex'}} >
                <div className="sidebar-videos">
                    <img style = {{display: 'inline-block'}} src={poster} alt="sidebar content" />
                </div>
                <div className="sidebar-header">
                    <h1>{title}</h1>
                    <p className='sidebar-header__channel'>{channel}</p>
                </div>
            </div>
            </Link>
        </section>
    )
}

export default SideCard
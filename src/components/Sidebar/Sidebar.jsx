import React from 'react';
import SideCard from '../../components/Sidebar/SideCard'
import "../Sidebar/Sidebar.scss"
import axios from 'axios';
import Player from '../../components/Video/Videoplayer';
import Comments from '../../components/Comments/Comments';
import Videodescription from '../../components/Video/Videodescription';
import CommentCard from '../../components/Comments/CommentCard';

class Sidebar extends React.Component {
    state = {
        videos: [],
        selectedVideo: {},
    }
  
    getSelectedVideo = (videoID) => {
        axios.get((`https://project-2-api.herokuapp.com/videos/${videoID}?api_key=9f449b1c-9af2-41c3-94af-3c78a13f7919`))
            .then((response) => {
                this.setState({
                    selectedVideo: response.data
                })
            })
    }
  
    componentDidMount() {
  
        axios.get("https://project-2-api.herokuapp.com/videos/?api_key=9f449b1c-9af2-41c3-94af-3c78a13f7919")
            .then((response) => {
                console.log(response.data)
                this.setState({
                    videos: response.data
                })
  
                const videoId = this.props.match.params.videoId || response.data[0].id
                console.log(videoId)
                this.getSelectedVideo(videoId)
            })
                .catch((error) => console.log(error))
        }

        componentDidUpdate(previousProps) {
          const previousVideoID = previousProps.match.params.videoId
          const currentVideoID = this.props.match.params.videoId
  
          console.log(previousVideoID === currentVideoID)
          
          if(previousVideoID !== currentVideoID) {
              this.getSelectedVideo(currentVideoID)
          }
      }
  
    render() {
    
        console.log(this.state.selectedVideo)

    if (!this.state.selectedVideo.id) return <div><p >Loading</p></div>

    let filteredVideos = this.state.videos.filter(
        video => video.id !== this.state.selectedVideo.id

    )
    return (
        <>
    <Player selectedVideo={this.state.selectedVideo} />
        < div className = "main_class" >
            <div className="container">
                <div className="inner_section">
                    <div className="upper_side_bar">
                        <Videodescription
                            selectedVideo={this.state.selectedVideo}
                        />
                        <Comments />
                        <CommentCard data={this.state.selectedVideo} />
                    </div>
                        <div className="upper-comments">
                            <h4 className="upper-comments__header">NEXT VIDEOS</h4>
                            <ul className="test">{filteredVideos.map(video => (<SideCard 
                                key={video.id} 
                                id={video.id}
                                title={video.title}
                                poster={video.image}
                                channel={video.channel} />))}
                            </ul>

                        </div>
                </div>
            </div>
            </div >
        </>
            );
        }
}
      
export default Sidebar;
      

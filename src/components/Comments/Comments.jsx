import React from 'react'
import image from "../../assets/images/icons/Mohan-muruge.jpg"
import "../../components/Comments/Comments.scss"


function comments() {

    const handleSubmit = (event) => {
        event.preventDefault()
    }

    return (
        <>
            <h3 className="comment-title">3 Comments</h3>
        <section className="comment-main">
         <div className="comment-form">
                    <div className="avatar-form__section">
                        <img className="avatar-form__image" src={image} alt="Avatar"/>
                    </div>
             <section className="comment-container">
                 <h4 className="comment-container__header">JOIN THE CONVERSATION</h4>
                    <form className="comment-form" action="" method="post">
                        <div className="upper_form">
                            <textarea className="comment-section__box" name="comment" id="comment" cols="10" placeholder="Add a new comment" rows="1"></textarea>
                        </div>
                        <div className='submit-comment__container'>
                         <button onSubmit={handleSubmit} className='submit-comment' type="submit">COMMENT</button>
                        </div>
                     </form>
             </section>       
            </div> 
        </section>
        </>
    )
}

export default comments


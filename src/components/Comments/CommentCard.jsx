const CommentCard = (props) => {
    return (
        <>
            <div className='main-comments'>

                {props.data.comments.map(comment => {
                    return (
                        <>
                            <div className="avatar_upper_div">
                                <div className="avatar_img">
                                    <div className="avatar-form__image"/>
                                </div>
                                <div className="avatar-form">
                                    <h2 className='main-comments__name'>{comment.name}</h2>
                                    <h3 className='main-comments__timestamp'>{new Date(comment.timestamp).toLocaleDateString()}</h3>
                                    <p className='main-comments__comments'>{comment.comment}</p>
                                </div>
                            </div>
                        </>
                )
             })}

            </div>
                
        </>
    )
}

export default CommentCard;
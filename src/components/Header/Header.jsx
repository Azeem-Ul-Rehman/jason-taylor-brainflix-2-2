import React from 'react';
import "./Header.scss";
import logo from "../../assets/images/logo/BrainFlix-logo.svg";
import image from "../../assets/images/icons/Mohan-muruge.jpg";
import search from "../../assets/images/icons/search.svg";
import { Link } from 'react-router-dom';


const Header = () => {
    return (
        <>
            <section className="header-main">
                <div className="container">
                    <div className="upper_header">
                        <div className="header__logo">
                        <Link to="/">
                                <img className="header__image" src={logo} alt="brainflix logo" />
                            </Link>
                        </div>
                    
                        <div className="header-area--right">
                            <div className="header__search">
                                <input className="header__search--input" type="text" placeholder="Search" img src={search} alt="search image" />
                                <div>
                                    <img className="header__icon-image desktop_non" src={image} alt="Mohan muruge" />
                                </div>
                            </div>
                            <Link to="/Upload" className='upload-link'>
                                <div>
                                    <button className="header__button" type="submit">UPLOAD</button>
                                </div>
                                </Link>
                            <div className="mobile_none">
                                <img className="header__icon-image mobile_non" src={image} alt="Mohan muruge" />
                            </div>
                        </div>
                    </div>
                </div>
            </section>
         </>
    )
}   

export default Header;




import React from 'react'
import Plane from '../../assets/images/airplane.jpg'
import '../Upload/Upload.scss'
import { Link } from 'react-router-dom';

const uploadSuccess = () => {
    alert(`Upload Complete`);
}


export default function upload() {
    
    return (
      
        <>
            <section className="upload-main">
                <div className="container">
                    <div>
                        <h3 className="upload-title">Upload Video</h3>
                    </div>
                    <div className="upper_upload">
                        <div className="upload-form">
                            <h4 className="upload-form__text">VIDEO THUMBNAIL</h4>
                            <img className="upload-avatar__image" src={Plane} alt="Avatar"/>
                        </div>
                        <div className="comment-container_upper">
                            <form className="" action="" method="post">
                                <h4 className="comment-container__header">TITLE YOUR VIDEO</h4>
                                <input className="upload-section__box--top" type="text" name="comment" id="comment"
                                    placeholder="Add a title to your video" />
                                <h4 className="comment-container__header">ADD A VIDEO DESCRIPTION</h4>
                                <input className="upload-section__box--bottom" type="text" name="comment" id="comment"
                                    placeholder="Add a description to your video" />
                            </form>
                        </div>
                    </div>
                    <div className="upload-publish__container">
                        <h4 className="upload-publish__cancel">CANCEL</h4>
                        <Link to="/" >
                            <button onClick={uploadSuccess}className="publish-comment__button" type="submit">PUBLISH</button>
                        </Link>
                    </div>
                </div>
            </section>    
        </>
    
    )
}

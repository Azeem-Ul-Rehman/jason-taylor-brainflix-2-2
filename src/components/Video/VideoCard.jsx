const VideoCard = (props) => {
    return (
        <div>
            <h1>{props.title}</h1>
            <h2>{props.likes}</h2>
            <h2>{props.views}</h2>
            <h3>{props.timestamp}</h3>
            <p>{props.comments}</p>
            <h2>{props.channel}</h2>
        </div>
    )
}

export default VideoCard;
import './Video.scss'

function videodescription (props) {
    const {channel, description, likes, views, title,timestamp} = props.selectedVideo;

        function formattedDate() {
        let date = new Date(timestamp)
        let day = date.getDate()
        let month = date.getMonth() + 1
        let year = date.getFullYear()

        let dateFormat = `${month}/${day}/${year}`

        return dateFormat
    }
    
    return(
        <div className="upper_video_title">
            <h1 className="video-title">{title}</h1>
                <div className='main-description__text'>
                    <div className="video-channel">
                        <h2>{channel} <span>{formattedDate()}</span> </h2>
                    </div>
                    <div className="inner_video_views">
                        <h3 className="video-views">{views}</h3>
                        <h3 className="video-likes">{likes}</h3>
                    </div>
                </div>
            <p className="video-description">{description}</p>
        </div>
    )
}

export default videodescription;

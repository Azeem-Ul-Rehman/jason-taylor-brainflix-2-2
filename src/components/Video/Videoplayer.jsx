const Player = (props) => {

  console.log(props)

    return (
      <section className="video-container">
        <div className="container padding_none">
          <div className="video-player__section">
            <video className="video-player__poster" poster={props.selectedVideo.image} controls>
              <source src = {props.selectedVideo.video} type = 'video/mp4'></source>
            </video>
          </div>
        </div>
      </section>
    )
}

export default Player;


import '../src/App.scss';
import React from 'react'
import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";
import Header from '../src/components/Header/Header';
import Upload from './components/Upload/Upload';
import Sidebar from './components/Sidebar/Sidebar'


class App extends React.Component{

  render(){
    
    return (
      <BrowserRouter>
      <div className="App">
        <Header />
        <Switch>
          <Route path="/" exact component={Sidebar}/>
              <Route path="/video/:videoId" component={Sidebar}/>
            <Route exact path="/Upload" component={Upload} />
            <Redirect to="/" component={Sidebar}/>
        </Switch>
      </div>
    </BrowserRouter>

  );
}
}

export default App;

